var passwordPack=new Array();
    var key;
    function retrievePack()
    {
        key = $("#key").val();
        jQuery.get('load.php', function(pack) {
            try {
                pack = zeroDecipher(key,pack);
            }
            catch (e)
            {
                alert ("Wrong key !");
                return false;
            }
            pack = JSON.parse(pack);
            if (pack.length == 0)
            {
                $("#message").html("Empty set");
            }
            passwordPack = pack;
            displayPassrot();
        });
        $("#actions").html('<a onclick="addNewPass()">Add new</a>');
        return false;
    }
    
    function initPack()
    {
        $("#initContent").val(zeroCipher($("#initpassword").val(),JSON.stringify(new Array())));
        return true;
    }
    
    function addNewPass()
    {
        $("#popup-new").show();
    }
    
    function displayPassrot()
    {
        if (passwordPack.length > 0)
        {
            $("#passrot").empty();
            nbPassword = passwordPack.length;
            for (i=0;i<nbPassword;i++)
            {
                passwordItem = JSON.parse(passwordPack[i]);
                var newline = "Name : "+passwordItem.name+" / Password : "+passwordItem.password;
                $("#passrot").append(newline+'<br />');
            }
        }
    }
    
    function save()
    {
        alert (zeroCipher(key,JSON.stringify(passwordPack)));
        $.post('save.php',{'pack' : zeroCipher(key,JSON.stringify(passwordPack))},function(data){$("#result").append(data)});
    }
    
    function insertNewPassword()
    {
        var newPass = {};
        newPass.name = $("#new-pass-name").val();
        newPass.password = $("#new-pass-password").val();
        passwordPack.push(JSON.stringify(newPass));
        save();
        return false;
    }