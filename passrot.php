<html>
<head>
<script src="lib/jquery.js#"></script>
<script src="lib/sjcl.js#"></script>
<script src="lib/base64.js#"></script>
<script src="lib/rawdeflate.js#"></script>
<script src="lib/rawinflate.js#"></script>
<script src="lib/zerobin.js#"></script>
<link rel="stylesheet" type="text/css" href="../css/main.css" />

<script src="lib/passrot.js"></script>
</head>
<body>
<div class="center">
    <div class="top-message" id="message">
        
    </div>
	<div class="block header">
            Passrot
        </div>
        <div id="popup-new" class="block">
            <form id="form-new-password" onsubmit="return insertNewPassword();">
                <input type="text" id="new-pass-name" />
                <input type="text" id="new-pass-password" />
                <input type="submit" />
            </form>
        </div>
        <div id="result"></div>
        <div class="block" id="passrot">
            <?php
            if (!file_exists('pack.php'))
            {
            ?>
                <p>The crypto database does not exist. Please choose your crypto key :</p>
                    <form action="init.php" method="post" onsubmit="return initPack();">
                        <input type="password" id="initpassword" name="initpassword" />
                        <input type="password" id="check_initpassword" name="check_initpassword" />
                        <input type="hidden" id="initContent" name="initcontent" value="" />
                        <input type="submit"/>
                    </form>
            <?php
            }
            else
            {
            ?>
            <form onsubmit="return retrievePack();">
            <input type="password" id="key" />
            </form>
            <?php
            }
            ?>
        </div>
        <div class="block" id="actions">
            
        </div>
        <script type="text/javascript">
            $("#popup-new").hide();
        </script>
</body>
</html>
